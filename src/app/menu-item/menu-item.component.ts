import { Component, OnInit, Input } from '@angular/core';
import {MenuItemInterface} from "../Interfaces/MenuItemInterface";
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['../Syles/demomenu.css'],
  animations: [
    trigger('menuDropDown', [

      transition('void => *', [
        style({transform: 'translateY(0)' , opacity:'0'}),
        animate(1000, style({transform: 'translateY(100vh)' , opacity:'1'}))
      ]),
      transition('* => void', [
        style({transform: 'translateY(100vh)' , opacity:'1'}),
        animate(1000,  style({transform: 'translateY(0)' , opacity:'0'}))
      ])
    ])
  ]
})
export class MenuItemComponent implements OnInit {
  @Input() menuItem: MenuItemInterface;
  subMenuItems: string[];
  isExpanded:boolean;
  constructor() {
    this.menuItem ={} as MenuItemInterface;
    this.subMenuItems =[];
    this.isExpanded=false;
  }

  ngOnInit(): void {
    this.subMenuItems = this.menuItem.subMenuItems;
  }

  expandMenu(){
    this.isExpanded ? this.isExpanded=false : this.isExpanded=true;
  }

}
