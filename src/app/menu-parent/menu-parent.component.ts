import { Component, OnInit } from '@angular/core';
import {HttpService} from "../http-service.service";
import {MenuItemComponent} from "../menu-item/menu-item.component";
import {MenuItemInterface} from "../Interfaces/MenuItemInterface";

@Component({
  selector: 'menu-parent',
  templateUrl: './menu-parent.component.html',
  styleUrls: ['../Syles/demomenu.css']
})
export class MenuParentComponent implements OnInit {
  menuResponse:any;
  menuItems: MenuItemInterface[];
  constructor(private httpService:HttpService) {
    this.menuItems=[];
  }

  ngOnInit(): void {
    this.httpService.getMenuJson().subscribe((response)=>{
      this.menuResponse = response;
      this.menuItems = this.menuResponse.MenuItems;
      console.log(this.menuItems);
    })


  }

}
