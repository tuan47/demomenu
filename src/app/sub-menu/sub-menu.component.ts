import { Component, OnInit, Input } from '@angular/core';
import {trigger, animate, style, transition, state} from "@angular/animations";

@Component({
  selector: 'sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.css'],
  animations: [
    trigger('menuDropDown', [

      transition('void => *', [
        style({height: '0%',overflow: 'hidden'}),
        animate(1000, style({height:'100%', overflow:'visible'}))
      ]),
      transition('* => void', [
         style({height: '100%',overflow: 'visible'}),
        animate(1000, style({height:'0%', overflow:'hidden'}))
      ])
    ])
  ]
})
export class SubMenuComponent implements OnInit {

  @Input() subMenuItem:string;
  constructor() {
    this.subMenuItem='';
  }

  ngOnInit(): void {
  }

}
